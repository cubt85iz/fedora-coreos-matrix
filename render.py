from jinja2 import Environment, FileSystemLoader
import yaml, argparse

# Define arguments for script
parser = argparse.ArgumentParser()
parser.add_argument("-t", "--template", type=str, required=True)
parser.add_argument("-o", "--output", type=str, required=False)

# Parse arguments into variables.
args = parser.parse_args()
template = args.template
output = args.output

# Load secrets from configuration file into dictionary.
secrets = yaml.safe_load(open('secrets.yml', 'r'))

# Open template
j2_environment = Environment(loader=FileSystemLoader("./"))
j2_template = j2_environment.get_template(template)

# Render template
j2_rendered_template = j2_template.render(secrets)

# Write rendered template to specified file.
if (output != None):
  with open(output, '+w') as file:
    file.write(j2_rendered_template)

# Otherwise, output to stdout
else:
  print(j2_rendered_template)
