.PHONY: all

all:
	@echo "Updating configuration"
	rm -rf ./config
	cp -a template config
	@echo
	@echo "Rendering templates"
	python render.py -t config/element-web/config.json.j2 -o config/element-web/config.json
	python render.py -t config/synapse/homeserver.yaml.j2 -o config/synapse/homeserver.yaml
	python render.py -t config/synapse/synapse.signing.key.j2 -o config/synapse/synapse.signing.key
	python render.py -t config/postgresql_synapse.j2 -o config/postgresql_synapse
	@echo
	@echo "Removing templates"
	rm config/{element-web/config.json.j2,synapse/{homeserver.yaml.j2,synapse.signing.key.j2},postgresql_synapse.j2}
	@echo
	@echo "Generating ignition configuration"
	python render.py -t config.bu.j2 | butane --files-dir config --strict --output config.ign
	@echo "Done"
